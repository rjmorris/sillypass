from collections import Counter
import random

import pytest

from sillypass import (
    DIGITS,
    LETTERS,
    LOWERCASE,
    SYMBOLS,
    UPPERCASE,
    create_counts,
    create_password,
)


@pytest.fixture(scope="module")
def rng():
    return random.Random()


def test_create_password_empty(rng):
    """
    If no character groups are specified, a blank password should be returned.
    """

    assert create_password({}, rng) == ""


@pytest.mark.parametrize(
    "counts",
    [
        {"a": 0},
        {"a": 1},
        {"a": 2},
        {"a": 0, "b": 0},
        {"a": 0, "b": 1},
        {"a": 2, "b": 0},
        {"a": 1, "b": 1},
        {"a": 1, "b": 2},
        {"a": 2, "b": 1},
        {"a": 2, "b": 3},
        {"a": 0, "b": 0, "c": 0},
        {"a": 0, "b": 0, "c": 1},
        {"a": 0, "b": 2, "c": 0},
        {"a": 1, "b": 2, "c": 0},
        {"a": 1, "b": 1, "c": 1},
        {"a": 1, "b": 1, "c": 2},
        {"a": 1, "b": 2, "c": 1},
        {"a": 2, "b": 1, "c": 1},
        {"a": 2, "b": 3, "c": 4},
    ],
)
def test_create_password_single(counts, rng):
    """
    When all the character groups are defined by a single character, we know
    ahead of time which characters should comprise the password and how many of
    each character should appear in the password. The order of the characters
    will be random, though, so we can't test that.
    """

    observed = create_password(counts, rng)
    expected = "".join(group[0] * counts[group] for group in counts)
    assert sorted(observed) == sorted(expected)


@pytest.mark.parametrize(
    "counts",
    [
        {"abc": 0},
        {"abc": 1},
        {"abc": 2},
        {"abc": 0, "def": 0},
        {"abc": 0, "def": 1},
        {"abc": 1, "def": 0},
        {"abc": 1, "def": 2},
        {"abc": 2, "def": 1},
        {"abc": 2, "def": 3},
        {"abc": 0, "def": 0, "ghi": 0},
        {"abc": 0, "def": 0, "ghi": 1},
        {"abc": 0, "def": 2, "ghi": 0},
        {"abc": 3, "def": 1, "ghi": 0},
        {"abc": 1, "def": 2, "ghi": 3},
        {"abc": 2, "def": 1, "ghi": 1},
        {"abc": 2, "def": 3, "ghi": 4},
    ],
)
def test_create_password_nonoverlapping(counts, rng):
    """
    When the character groups are defined by multiple characters, then we can't
    know ahead of time which of those characters will appear in the password or
    how many times each individual character will appear. All we can know is the
    total number of times the characters will appear collectively. With multiple
    groups, as long as their characters don't overlap, we can verify the
    collective count independently for each group.
    """

    observed = create_password(counts, rng)
    observed_counts = Counter(observed)

    for group in counts:
        observed_count = sum(observed_counts[c] for c in group)
        assert observed_count == counts[group]


@pytest.mark.parametrize(
    "group1, group2",
    [
        ("abc", "cde"),
        ("abc", "bcd"),
        ("abc", "abc"),
    ],
)
@pytest.mark.parametrize(
    "count1, count2",
    [
        (0, 0),
        (0, 1),
        (1, 0),
        (1, 1),
        (1, 2),
        (2, 2),
        (2, 3),
    ],
)
def test_create_password_overlapping(group1, group2, count1, count2, rng):
    """
    When multiple character groups are provided and they share some characters,
    we can put an upper bound on the number of times each character should
    appear in the password. The upper bound will be the sum of the required
    count for each group containing the character. For example, if the character
    "a" appears in the 1st and 3rd group but not the 2nd, then "a" should appear
    in the password no more times than the sum of the counts for the 1st and 3rd
    groups.

    We can make the upper bound even tighter by applying it to a collection of
    characters instead of to each character individually, assuming the
    collection of characters appears in exactly the same groups. For example, if
    characters "a" and "b" both appear in the 1st and 3rd group and neither
    appears in the 2nd, then they appear in exactly the same groups. If we add
    the number of times "a" appears in the password to the number of times "b"
    appears in the password, the upper bound should be the sum of the counts for
    the 1st and 3rd groups.
    """

    counts = {
        group1: count1,
        group2: count2,
    }

    observed = create_password(counts, rng)
    observed_counts = Counter(observed)

    both = set(group1) & set(group1)
    group1_only = set(group1) - set(group2)
    group2_only = set(group2) - set(group1)

    assert sum(observed_counts[c] for c in both) <= count1 + count2
    assert sum(observed_counts[c] for c in group1_only) <= count1
    assert sum(observed_counts[c] for c in group2_only) <= count2


@pytest.mark.parametrize(
    "args",
    [
        {"length": -1},
        {"min_letters": -2},
        {"min_upper": -3},
        {"min_lower": -4},
        {"min_digits": -5},
        {"min_symbols": -6},
    ],
)
def test_create_counts_negative(args):
    """
    Passing a negative value to one of the count parameters should raise an
    exception.
    """

    with pytest.raises(ValueError):
        create_counts(**args)


@pytest.mark.parametrize(
    "args",
    [
        {"length": 0, "min_letters": 1},
        {"length": 1, "min_letters": 2},
        {"length": 2, "min_letters": 3},
        {"length": 2, "min_upper": 3},
        {"length": 2, "min_lower": 3},
        {"length": 2, "min_digits": 3},
        {"length": 2, "min_symbols": 3},
        {"length": 2, "min_letters": 1, "min_digits": 1, "min_symbols": 1},
    ],
)
def test_create_counts_overflow(args):
    """
    If the sum of the minimum counts for each group exceed the total length, an
    exception should be raised.
    """

    with pytest.raises(ValueError):
        create_counts(**args)


def test_create_counts_empty():
    """
    Specifying a 0-length password should produce an empty dictionary of counts.
    """

    counts = create_counts(length=0)
    assert counts == {}


@pytest.mark.parametrize("length", (0, 1, 2, 12, 24, 128))
def test_create_counts_length(length):
    """
    The sum of the counts should match the specified length.
    """

    counts = create_counts(length=length)
    assert length == sum(counts[group] for group in counts)


@pytest.mark.parametrize(
    "case",
    [
        {"arg": "min_letters", "group": LETTERS},
        {"arg": "min_upper", "group": UPPERCASE},
        {"arg": "min_lower", "group": LOWERCASE},
        {"arg": "min_digits", "group": DIGITS},
        {"arg": "min_symbols", "group": SYMBOLS},
    ],
)
def test_create_counts_groups(case, rng):
    """
    The count assigned to a character group in the returned dictionary should
    match the value passed to the corresponding argument. For example, if
    min_letters=5, then counts[LETTERS] should be 5.
    """

    length = 24
    min_count = rng.randint(1, length)
    args = {
        "length": length,
        case["arg"]: min_count,
    }
    counts = create_counts(**args)
    assert case["group"] in counts
    assert counts[case["group"]] == min_count


def test_create_counts_remainder_default():
    """
    If not otherwise specified, a remainder character group matching the union
    of letters, digits, and symbols should be included in the counts.
    """

    counts = create_counts()
    remainder = set(LETTERS) | set(DIGITS) | set(SYMBOLS)
    assert len([group for group in counts if set(group) == remainder]) >= 1


def test_create_counts_remainder_specified():
    """
    If a remainder group is specified, it should be included in the counts
    instead of the default remainder group.
    """

    remainder = "sillypass"
    counts = create_counts(remainder=remainder)
    assert remainder in counts

    default_remainder = set(LETTERS) | set(DIGITS) | set(SYMBOLS)
    assert (
        len([group for group in counts if set(group) == default_remainder]) == 0
    )


@pytest.mark.parametrize(
    "args",
    [
        {},
        {
            "min_letters": 1,
        },
        {
            "min_upper": 2,
            "min_lower": 4,
        },
        {
            "min_letters": 1,
            "min_digits": 8,
            "min_symbols": 16,
        },
        {
            "min_letters": 1,
            "min_upper": 2,
            "min_lower": 4,
            "min_digits": 8,
            "min_symbols": 16,
        },
    ],
)
def test_create_counts_remainder_count(args):
    """
    The count assigned to the remainder character group should equal the
    difference between the total length and the sum of the other character
    groups' counts.
    """

    length = 64
    remainder = "sillypass"
    counts = create_counts(length=length, remainder=remainder, **args)
    observed = counts[remainder]
    expected = length - sum(args[arg] for arg in args)
    assert observed == expected


def test_create_counts_symbols_specified():
    """
    If a group of symbols is specified along with a minimum symbol count, the
    specified symbol group should be included in the counts instead of the
    default symbol group.
    """

    symbols = "!@#$"
    counts = create_counts(symbols=symbols, min_symbols=1)
    assert symbols in counts

    default_symbols = SYMBOLS
    assert default_symbols not in counts


def test_create_counts_symbols_specified_remainder():
    """
    If a group of symbols is specified, the remainder group should include the
    specified symbols and not the default symbols.
    """

    symbols = "!@#$"
    counts = create_counts(symbols=symbols)

    remainder = set(LETTERS) | set(DIGITS) | set(symbols)
    assert len([group for group in counts if set(group) == remainder]) >= 1

    default_remainder = set(LETTERS) | set(DIGITS) | set(SYMBOLS)
    assert (
        len([group for group in counts if set(group) == default_remainder]) == 0
    )
