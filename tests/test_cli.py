import argparse
import subprocess

import pytest

from sillypass.__main__ import count


def test_run_script():
    """
    The package should be runnable from the shell as a script.
    """
    subprocess.run(["sillypass"], check=True)


def test_run_module():
    """
    The package should be runnable from the shell as a module.
    """
    subprocess.run(["python", "-m", "sillypass"], check=True)


def test_count_negative():
    """
    count() should raise a ArgumentTypeError if provided a negative number.
    """
    with pytest.raises(argparse.ArgumentTypeError):
        count("-1")


@pytest.mark.parametrize("value", ("a", "1.234", "true", ""))
def test_count_nonint(value):
    """
    count() should raise a ArgumentTypeError if provided a string that can't be
    cast to an integer.
    """
    with pytest.raises(argparse.ArgumentTypeError):
        count(value)


@pytest.mark.parametrize("value", ("0", "1", "2", "10"))
def test_count_int(value):
    """
    count() should return an integer version of its argument.
    """
    assert count(value) == int(value)
