ARG PYTHON_VERSION=3.8
FROM python:${PYTHON_VERSION}

WORKDIR /code

COPY ./requirements.txt ./
RUN pip install --upgrade pip \
    && pip install -r requirements.txt

COPY ./ ./
