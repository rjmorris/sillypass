# Changelog

This documents outlines the release history for the project. Version numbers follow the [semantic versioning](https://semver.org) convention.

## [1.0.0] - 2020-07-31

Initial release, including the following features:

- Provides a command-line tool to generate a random password.
- Supports specifying the length of the password.
- Supports specifying the minimum number of uppercase letters, lowercase letters, mixed-case letters, digits, and symbols in the password.
- Supports specifying a custom set of symbols to choose from.
