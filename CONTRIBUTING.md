# Contributing to sillypass

Issues and merge requests are welcomed!

## Development environment

A `requirements.txt` file is provided for creating a reproducible Python virtual environment for the purposes of development and testing. To install dependencies into a virtual environment, first create and activate the virtual environment, then run `pip install -r requirements.txt`.

A Docker setup is provided if you'd prefer to work in a Docker container instead of in a native virtual environment. To install dependencies using Docker, build the Docker image with `docker compose build`. This builds images for all the Python versions supported by `sillypass`. Or build an image for a single Python version like `docker compose build py312`.

Whether you're developing natively or through Docker, you may wish to install the `sillypass` package itself with `pip install -e .`. In the case of Docker, you'll need to do this inside a running container, and you'll need to do it each time you start a new container.

## Testing, formatting, linting

Several tools are used for testing, formatting, and linting the source code. Each of these is run automatically as part of the GitLab continuous integration (CI) pipeline, but you're also encouraged to run them yourself to fix any errors before pushing to the central repository where they will cause the CI pipeline to fail. See `.gitlab-ci.yml` for a list of the tools used and how they're invoked.

Take pytest for example. Run it from a local virtual environment with:

```
$ pip install -e .  # if you haven't already installed the package
$ pytest
```

Or run it in a Docker container with:

```
$ docker compose run --rm py312 bash -c "pip install . && pytest"
```

Installing the package before running pytest is recommended, as demonstrated in the above examples, but it may not be necessary for some of the other tools like ruff.

A [pre-commit](https://pre-commit.com) configuration is provided for running some of these checks automatically before every commit. This is optional but encouraged so that running the checks before pushing is more convenient than remembering to do it manually. Enable the pre-commit checks by running `pre-commit install` inside your activated virtual environment. After that, you will be prevented from committing your changes if any of the checks fail. If for some reason you really need to commit changes despite failing checks, run `git commit --no-verify`.

## Managing requirements

[pip-tools](https://github.com/jazzband/pip-tools) is used for managing `requirements.txt`. To edit the development dependencies:

1. Edit the top-level dependencies in `requirements.in`.
1. Run `pip-compile` to generate a new `requirements.txt`.

To upgrade a particular development dependency, run `pip-compile --upgrade-package package-name` to generate a new `requirements.txt`.

After generating a new `requirements.txt`, you may wish to update your virtual environment. To do that:

1. Run `pip-sync` to sync the packages installed your virtual environment with the ones specified in `requirements.txt`.
1. Re-install `sillypass` into your virtual environment with `pip install -e .`, because `pip-sync` uninstalls it.

## Publishing

To publish a new release:

1. Bump the version number in `setup.py`.
1. Update `CHANGELOG.md` to describe the new release.
1. Tag the version in git with `git tag -a v1.0.0 -m "initial release"`.
1. Push your changes and the tag with `git push origin --follow-tags`.
1. Verify that the CI pipeline succeeded.
1. Clean up any lingering build artifacts with `rm -rf build/ dist/ src/sillypass.egg-info/`.
1. Build a new source distribution and wheel with `python -m build`.
1. Upload to TestPyPI with `twine upload -r testpypi dist/*`.
1. Review the package page on TestPyPI to make sure there are no errors.
1. Install the package from TestPyPI in a fresh virtual environment with `pip install -i https://test.pypi.org/simple/ sillypass` to make sure you can install and run it.
1. Upload to PyPI with `twine upload dist/*`.
1. Review the package page on PyPI to make sure there are no errors.
1. Install the package from PyPI in a fresh virtual environment with `pip install sillypass` to make sure you can install and run it.
